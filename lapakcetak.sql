-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2018 at 07:23 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lapakcetak`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_alamat`
--

CREATE TABLE `t_alamat` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_alamat` varchar(100) NOT NULL,
  `nama_penerima` varchar(100) NOT NULL,
  `no_telp` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `kecamatan` varchar(100) NOT NULL,
  `kode_pos` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_alamat`
--

INSERT INTO `t_alamat` (`id`, `id_user`, `nama_alamat`, `nama_penerima`, `no_telp`, `alamat`, `provinsi`, `kota`, `kecamatan`, `kode_pos`) VALUES
(1, 3, 'Rumah', 'Fakhrizal Andyko', '08972572573', 'Perum Nirmala Asri Blok A No.2', 'Jawa Barat', 'Bekasi', 'Jatiranggon', 17432),
(3, 3, 'Rumah 2', 'Alan Wiegner', '08972572573', 'Perum Nirmala Asri blok A No.2', 'Jawa Barat', 'Bekasi', 'Jatiranggon', 17432),
(4, 3, 'Rumah 2', 'Alan Wiegner', '08972572573', 'Perum Nirmala Asri blok A No.2', 'Jawa Barat', 'Bekasi', 'Jatiranggon', 17432);

-- --------------------------------------------------------

--
-- Table structure for table `t_bahan`
--

CREATE TABLE `t_bahan` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `harga` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_bahan`
--

INSERT INTO `t_bahan` (`id`, `nama`, `description`, `harga`) VALUES
(1, 'Cotton', '100% bahan katun jersey', 132000);

-- --------------------------------------------------------

--
-- Table structure for table `t_detail_kaos`
--

CREATE TABLE `t_detail_kaos` (
  `id` int(11) NOT NULL,
  `id_detail` int(11) NOT NULL,
  `id_warna` int(11) NOT NULL,
  `id_ukuran` int(11) NOT NULL,
  `id_bahan` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `file_url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_detail_kaos`
--

INSERT INTO `t_detail_kaos` (`id`, `id_detail`, `id_warna`, `id_ukuran`, `id_bahan`, `jumlah`, `file_url`) VALUES
(1, 6, 2, 4, 1, 6, 'sadsadasdasd'),
(2, 8, 2, 4, 1, 6, 'sadsadasdasd'),
(3, 9, 1, 3, 1, 3, 'awdasdawdasdad');

-- --------------------------------------------------------

--
-- Table structure for table `t_detail_pemesanan`
--

CREATE TABLE `t_detail_pemesanan` (
  `id_detail` int(11) NOT NULL,
  `id_pemesanan` varchar(50) NOT NULL,
  `id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_detail_pemesanan`
--

INSERT INTO `t_detail_pemesanan` (`id_detail`, `id_pemesanan`, `id_produk`) VALUES
(6, 'nEXAAdPx', 1),
(8, '3vcXKpFO', 1),
(9, '3vcXKpFO', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_detail_poster`
--

CREATE TABLE `t_detail_poster` (
  `id` int(11) NOT NULL,
  `id_detail` int(11) NOT NULL,
  `spek_kertas` int(11) NOT NULL,
  `jumlah` int(100) NOT NULL,
  `file_url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_pemesanan`
--

CREATE TABLE `t_pemesanan` (
  `id` varchar(50) NOT NULL,
  `id_alamat` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('payment','process','delivery','finished','cancelled') NOT NULL DEFAULT 'payment',
  `payment_status` enum('N','Y') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pemesanan`
--

INSERT INTO `t_pemesanan` (`id`, `id_alamat`, `tanggal`, `status`, `payment_status`) VALUES
('3vcXKpFO', 4, '2018-07-17 02:23:07', 'payment', 'N'),
('nEXAAdPx', 1, '2018-07-17 02:19:28', 'payment', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `t_produk`
--

CREATE TABLE `t_produk` (
  `id_produk` int(11) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `gambarPlain` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  `link` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_produk`
--

INSERT INTO `t_produk` (`id_produk`, `nama_produk`, `gambar`, `gambarPlain`, `type`, `link`) VALUES
(1, 'Kaos', 'kaos.jpg', 'plain-tshirt.png', 'kaos', 'tshirt'),
(2, 'Banner', 'banner.jpg', '', '', 'banner'),
(3, 'Poster', 'poster.jpg', '', '', 'poster');

-- --------------------------------------------------------

--
-- Table structure for table `t_ukuran_kaos`
--

CREATE TABLE `t_ukuran_kaos` (
  `id` int(11) NOT NULL,
  `kode` varchar(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `harga` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_ukuran_kaos`
--

INSERT INTO `t_ukuran_kaos` (`id`, `kode`, `nama`, `harga`) VALUES
(1, 'S', 'Small', 0),
(3, 'M', 'Medium', 0),
(4, 'L', 'Large', 5000);

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id_user` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `usertype` enum('member','admin','suspended') NOT NULL,
  `last_login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id_user`, `email`, `username`, `password`, `nama_lengkap`, `no_hp`, `usertype`, `last_login`) VALUES
(3, 'fary290796@gmail.com', 'faryzal2020', 'a1b2c3d4e5', 'Fakhrizal Andyko', '08972572573', 'member', '2018-07-10 13:01:11');

-- --------------------------------------------------------

--
-- Table structure for table `t_warna`
--

CREATE TABLE `t_warna` (
  `id_warna` int(11) NOT NULL,
  `hex_code` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_warna`
--

INSERT INTO `t_warna` (`id_warna`, `hex_code`) VALUES
(1, '4286f4'),
(2, 'f45042'),
(3, 'ffffff'),
(4, '3a3a3a'),
(5, '90d157');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_alamat`
--
ALTER TABLE `t_alamat`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `t_bahan`
--
ALTER TABLE `t_bahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_detail_kaos`
--
ALTER TABLE `t_detail_kaos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `t_detail_pemesanan`
--
ALTER TABLE `t_detail_pemesanan`
  ADD PRIMARY KEY (`id_detail`),
  ADD UNIQUE KEY `id_detail` (`id_detail`);

--
-- Indexes for table `t_detail_poster`
--
ALTER TABLE `t_detail_poster`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `t_pemesanan`
--
ALTER TABLE `t_pemesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_produk`
--
ALTER TABLE `t_produk`
  ADD PRIMARY KEY (`id_produk`),
  ADD UNIQUE KEY `link` (`link`);

--
-- Indexes for table `t_ukuran_kaos`
--
ALTER TABLE `t_ukuran_kaos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `id_user` (`id_user`);

--
-- Indexes for table `t_warna`
--
ALTER TABLE `t_warna`
  ADD PRIMARY KEY (`id_warna`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_alamat`
--
ALTER TABLE `t_alamat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `t_bahan`
--
ALTER TABLE `t_bahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_detail_kaos`
--
ALTER TABLE `t_detail_kaos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_detail_pemesanan`
--
ALTER TABLE `t_detail_pemesanan`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `t_detail_poster`
--
ALTER TABLE `t_detail_poster`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_produk`
--
ALTER TABLE `t_produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_ukuran_kaos`
--
ALTER TABLE `t_ukuran_kaos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_warna`
--
ALTER TABLE `t_warna`
  MODIFY `id_warna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
