<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tshirt extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('mglobal');
		$this->load->model('mtshirt');
		$this->load->library('session');
	}

	public function index(){
		$data['products'] = $this->mglobal->getListProduk();
		$data['ukuran'] = $this->mtshirt->getListUkuran();
		$data['warna'] = $this->mtshirt->getListWarna();
		$data['bahan'] = $this->mtshirt->getListBahan();
		$data['content_id'] = "pages/f-tshirt";
		$this->load->view("template/headerfooter", $data);
	}

	public function do_Login(){
		$this->mglobal->proses_login();
	}

	public function getPrice(){
		$type = $this->input->post('type');
		if($type == "tshirt"){
			$bahan = $this->mtshirt->getBahanPrice($this->input->post('bahan'));
			$ukuran = $this->mtshirt->getUkuranPrice($this->input->post('ukuran'));
			echo json_encode(['bahan' => $bahan,'ukuran' => $ukuran]);
		}
	}
}
