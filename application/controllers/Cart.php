<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('mglobal');
		$this->load->model('mcart');
		$this->load->library('session');
	}

	public function index(){
		$data['products'] = $this->mglobal->getListProduk();
		$data['content_id'] = "pages/f-cart";
		$this->load->view("template/headerfooter", $data);
	}

	public function do_Login(){
		$this->mglobal->proses_login();
	}

	public function loadCartTable(){
		$type = $this->input->post('type');
		$product = $this->mcart->getProductById($type);
		if($type == "tshirt"){
			$bahan = $this->mcart->getBahanByID($this->input->post('bahan'));
			$ukuran = $this->mcart->getUkuranByID($this->input->post('ukuran'));
			$warna = $this->mcart->getWarnaByID($this->input->post('warna'));

			echo json_encode([
				'bahan' => $bahan,
				'ukuran' => $ukuran,
				'warna' => $warna,
				'jumlah' => $this->input->post('jumlah'),
				'cartid' => $this->input->post('cartid'),
				'product' => $product
			]);
		}
		//echo json_encode(['data' => $data,'detailedCart' => $cart]);
	}
}
