<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('mglobal');
		$this->load->model('mmain');
		$this->load->library('session');
	}

	public function index(){
		$data['products'] = $this->mglobal->getListProduk();
		$data['content_id'] = "homepage/f-homepage";
		$this->load->view("template/headerfooter", $data);
	}

	public function do_Login(){
		$this->mglobal->proses_login();
	}

	public function do_Logout()
	{
		$userdata = array (
				'username'	=> '',
				'nama'    	=> '',
				'no_hp'		=> '',
				'email'		=> '',
				'usertype'	=> '',
				'is_login'	=> ''
			);
		$this->session->unset_userdata($userdata);
		$this->session->sess_destroy();
		redirect(base_url());
    }

	public function Register(){
	}
}
