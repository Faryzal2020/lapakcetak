<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('mmain');
	}

	public function index(){
		$data['content_id'] = "pages/f-tshirt";
		$this->load->view("template/headerfooter", $data);
	}
}
