<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('mglobal');
		$this->load->model('morders');
		$this->load->library('session');
	}

	public function index(){
		$data['products'] = $this->mglobal->getListProduk();
		$data['isLoggedIn'] = $this->session->userdata('is_login');
		if($data['isLoggedIn']){
			$data['orders'] = $this->morders->getListOrder();
		}
		$data['content_id'] = "pages/f-orders";
		$this->load->view("template/headerfooter", $data);
	}
}
