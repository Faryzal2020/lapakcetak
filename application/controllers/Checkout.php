<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('mglobal');
		$this->load->model('mcheckout');
		$this->load->library('session');
	}

	public function index(){
		$data['products'] = $this->mglobal->getListProduk();
		$data['isLoggedIn'] = $this->session->userdata('is_login');
		$data['addressList'] = $this->mcheckout->getAddressList();
		$data['content_id'] = "pages/f-checkout";
		$this->load->view("template/headerfooter", $data);
	}

	public function do_Login(){
		$this->mglobal->proses_login();
	}

	public function loadCartTable(){
		$type = $this->input->post('type');
		if($type == "tshirt"){
			$bahan = $this->mcheckout->getBahanByID($this->input->post('bahan'));
			$ukuran = $this->mcheckout->getUkuranByID($this->input->post('ukuran'));
			$warna = $this->mcheckout->getWarnaByID($this->input->post('warna'));

			echo json_encode(['bahan' => $bahan,'ukuran' => $ukuran,'warna' => $warna,'jumlah' => $this->input->post('jumlah'), 'cartid' => $this->input->post('cartid')]);
		}
		//echo json_encode(['data' => $data,'detailedCart' => $cart]);
	}

	public function loadAddressData(){
		$data = $this->mcheckout->getAddressById();
		echo json_encode($data);
	}

	public function createOrder(){
		$data = json_decode($this->input->post('data'));
		$cart = $data->cart;
		if($data->dataAlamat == "new"){
			$AddressId = $this->mcheckout->newAddress();
		} else {
			$AddressId = $data->dataAlamat;
		}
		$order = $this->mcheckout->do_order($AddressId);
		echo json_encode(['data'=>$data,'cart'=>$cart,'order'=>$order]);
	}
}
