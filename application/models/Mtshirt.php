<?php
class Mtshirt extends CI_Model {

	public function getListUkuran(){
		$db = $this->load->database("default", TRUE);
		$query = $db->query("SELECT * FROM t_ukuran_kaos");
		if($query){
			return $query->result();
		} else {
			return false;
		}
	}

	public function getListWarna(){
		$db = $this->load->database("default", TRUE);
		$query = $db->query("SELECT * FROM t_warna");
		if($query){
			return $query->result();
		} else {
			return false;
		}
	}

	public function getListBahan(){
		$db = $this->load->database("default", TRUE);
		$query = $db->query("SELECT * FROM t_bahan");
		if($query){
			return $query->result();
		} else {
			return false;
		}
	}

	public function getBahanPrice($id){
		$db = $this->load->database("default", TRUE);
		$query = $db->query("SELECT * FROM t_bahan WHERE id = '$id'");
		if($query){
			return $query->row()->harga;
		} else {
			return false;
		}
	}

	public function getUkuranPrice($id){
		$db = $this->load->database("default", TRUE);
		$query = $db->query("SELECT * FROM t_ukuran_kaos WHERE id = '$id'");
		if($query){
			return $query->row()->harga;
		} else {
			return false;
		}
	}
}