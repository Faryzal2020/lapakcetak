<?php
class Morders extends CI_Model {
	
	public function getListOrder(){
		$db = $this->load->database("default", TRUE);
		$id_user = $this->getUserId();
		$query = $db->query("SELECT t_pemesanan.* FROM t_pemesanan, t_alamat, t_user 
			WHERE t_pemesanan.id_alamat = t_alamat.id 
			AND t_user.id_user = t_alamat.id_user 
			AND t_user.id_user = '$id_user'");
		$order_array = array();
		foreach($query->result() as $row1){
			$detail_array = array();
			$query2 = $db->query("SELECT t_detail_pemesanan.*,t_produk.type,t_produk.nama_produk FROM t_detail_pemesanan, t_produk
				WHERE t_detail_pemesanan.id_pemesanan = '$row1->id'
				AND t_produk.id_produk = t_detail_pemesanan.id_produk");

			foreach($query2->result() as $row2){
				$tablename = "t_detail_".$row2->type;
				$query3 = $db->query("SELECT * FROM ".$tablename." WHERE id_detail = '$row2->id_detail'");
				foreach($query3->result() as $row3){ // one on one query with $row2
					$detail_array[] = array(
							'id_detail' => $row2->id_detail,
							'productname' => $row2->nama_produk,
							'jumlah' => $row3->jumlah
						);
				}
			}
			$order_array[] = array(
					'id' => $row1->id,
					'tanggal' => $row1->tanggal,
					'status' => $row1->status,
					'payment' => $row1->payment_status,
					'items' => $detail_array
				);
		}
		return $order_array;
	}

	function getUserId(){
		$db = $this->load->database("default", TRUE);
		$username = $this->session->userdata('username');
		$query = $db->query("SELECT * FROM t_user WHERE username = '$username'");
		if($query){
			return $query->row()->id_user;
		}
	}

	function randString($length) {
	    $char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	    $char = str_shuffle($char);
	    for($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i ++) {
	        $rand .= $char{mt_rand(0, $l)};
	    }
	    return $rand;
	}
}