<?php
class Mcheckout extends CI_Model {
	public function getBahanByID($id){
		$db = $this->load->database("default", TRUE);
		$query = $db->query("SELECT * FROM t_bahan WHERE id = '$id'");
		if($query){
			return $query->result();
		} else {
			return false;
		}
	}
	public function getUkuranByID($id){
		$db = $this->load->database("default", TRUE);
		$query = $db->query("SELECT * FROM t_ukuran_kaos WHERE id = '$id'");
		if($query){
			return $query->result();
		} else {
			return false;
		}
	}
	public function getWarnaByID($id){
		$db = $this->load->database("default", TRUE);
		$query = $db->query("SELECT * FROM t_warna WHERE id_warna = '$id'");
		if($query){
			return $query->result();
		} else {
			return false;
		}
	}
	public function getAddressList(){
		$username = $this->session->userdata('username');
		$db = $this->load->database("default", TRUE);
		$query = $db->query("SELECT t_alamat.* FROM t_alamat, t_user WHERE t_user.id_user = t_alamat.id_user AND t_user.username = '$username'");
		if($query){
			return $query->result();
		}
	}
	public function getAddressById(){
		$db = $this->load->database("default", TRUE);
		$id = $this->input->post('id');
		$username = $this->session->userdata('username');
		$query = $db->query("SELECT t_alamat.* FROM t_alamat, t_user WHERE t_user.id_user = t_alamat.id_user AND t_user.username = '$username' AND t_alamat.id = '$id'");
		if($query){
			return $query->result();
		}
	}
	public function newAddress(){
		$db = $this->load->database("default", TRUE);
		$id_user = $this->getUserId();
		$data = json_decode($this->input->post('data'));
		$query = $db->query("INSERT INTO t_alamat (id_user,nama_alamat,nama_penerima,no_telp,alamat,provinsi,kota,kecamatan,kode_pos) 
			VALUES ('$id_user','$data->namaAlamat','$data->nama','$data->telp','$data->alamat','$data->provinsi','$data->kota','$data->kecamatan','$data->kodepos')");
		$query = $db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		return $row['LAST_INSERT_ID()'];
	}

	public function do_order($addressid){
		$db = $this->load->database("default", TRUE);
		$data = json_decode($this->input->post('data'));
		$idOrder = $this->randString(8);
		$query = $db->query("INSERT INTO t_pemesanan (id,id_alamat) VALUES ('$idOrder','$addressid')");
		if($query){
			foreach ($data->cart as $i => $r) {
				$idProduct = $this->getProductId($r->type);
				$query = $db->query("INSERT INTO t_detail_pemesanan (id_pemesanan,id_produk) VALUES ('$idOrder','$idProduct')");
				if($query){
					$query = $db->query('SELECT LAST_INSERT_ID()');
				    $row = $query->row_array();
					$id_detail = $row['LAST_INSERT_ID()'];
					$type = $this->getProductType($idProduct);
					if($type == "kaos"){
						if($query){
							$query = $db->query("INSERT INTO t_detail_kaos (id_detail,id_warna,id_ukuran,id_bahan,jumlah,file_url) 
							VALUES ('$id_detail','$r->warna','$r->ukuran','$r->bahan','$r->jumlah','$r->fileurl')");
						}
					}
				}
			}
		}
		return $query;
	}

	function getProductId($link){
		$db = $this->load->database("default", TRUE);
		$query = $db->query("SELECT * FROM t_produk WHERE link = '$link'");
		if($query){
			return $query->row()->id_produk;
		}
	}
	function getProductType($id){
		$db = $this->load->database("default", TRUE);
		$query = $db->query("SELECT * FROM t_produk WHERE id_produk = '$id'");
		if($query){
			return $query->row()->type;
		}
	}

	function getUserId(){
		$db = $this->load->database("default", TRUE);
		$username = $this->session->userdata('username');
		$query = $db->query("SELECT * FROM t_user WHERE username = '$username'");
		if($query){
			return $query->row()->id_user;
		}
	}

	function randString($length) {
	    $char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	    $char = str_shuffle($char);
	    for($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i ++) {
	        $rand .= $char{mt_rand(0, $l)};
	    }
	    return $rand;
	}
}