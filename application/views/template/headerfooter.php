<!DOCTYPE html>
	<head>
		<meta charset="utf-8" />
		<title>LapakCetak</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css" />

		<!-- JS Stuff -->
		<script type="text/javascript" src="<?=base_url()?>file/js/jquery.js"></script>
		<script type="text/javascript" src="<?=base_url()?>file/js/jquery-ui.js"></script>
		<script type="text/javascript" src="<?=base_url()?>file/js/bootstrap.js"></script>

		<!-- CSS Stuff -->
		<link href="<?=base_url()?>file/css/bootstrap.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>file/css/jquery-ui.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>file/css/style.css" rel="stylesheet" type="text/css" />

		<body>
			<div class="navbar-block" style="position: fixed; top: 0px; width:100%; z-index: 1;">
				<div class="row">
					<div class="col-sm-12 top-block">
						<div class="container" style="text-align: right; font-size: 0.8em;">
							Contact Us
						</div>
					</div>
				</div>
				<div class="navbar navbar-default">
					<div class="container" style="padding: 0px 50px 0px 50px; ">
						<a class="navbar-header" style="cursor: pointer;" href="<?=base_url();?>">
							<span class="logo">LapakCetak</span>
						</a>
						<ul class="nav navbar-nav">
							<li>
								<a class="dropdown">Produk Kami</a>
								<div class="dropdown-panel" style="padding: 10px 0px;">
									<?php foreach($products as $row){?>
									<ul style="list-style-type: none; padding-left: 0px;">
										<li style="padding: 10px 20px;">
											<a href="<?=base_url().$row->link?>">
												<span><?php echo $row->nama_produk;?></span>
											</a>
										</li>
									</ul>
									<?php } ?>
								</div>
								<div class="dropdown-block"></div>
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li>
								<?php 
									$is_logged_in = $this->session->userdata('is_login');
									if( isset($is_logged_in) && $is_logged_in == 1){
								?>
								<a class="dropdown"><?php echo $this->session->userdata('nama');?></a>
								<div class="dropdown-panel" style="padding: 0px;">
									<ul style="padding-left: 0px; list-style-type: none;">
										<li><a href="<?=base_url().'orders'?>">Daftar transaksi</a></li>
										<li id="logoutBtn"><a href="">Logout</a></li>
									</ul>
								</div>
								<?php } else { ?>
								<a class="dropdown"><span class="glyphicon glyphicon-log-in"></span> Login</a>
								<div class="dropdown-panel" style="padding: 20px 45px;">
									<form class="form-horizontal" id="formLogin">
										<div class="form-group">
											<label for="usr">Username:</label>
											<input type="text" class="form-control" name="usr" id="usr">
										</div>
										<div class="form-group">
											<label for="pwd">Password:</label>
											<input type="password" class="form-control" name="pwd" id="pwd">
										</div>
										<button id="loginBtn" class="btn btn-default" style="position: relative; left: -15px;">Login</button>
									</form>
								</div>
								<?php } ?>
								<div class="dropdown-block"></div>
							</li>
							<li>
								<div style="height: 30px; margin-top: 5px; border-left: 1px solid white;"></div>
							</li>
							<?php if($this->session->userdata('is_login')){ ?>
							<li>
								<a class="cart" href="<?=base_url().'cart'?>"> <span class="glyphicon glyphicon-shopping-cart"></span><span id="cartItemCount" style="padding-left: 8px;"></span></a>
								<div class="dropdown-block"></div>
							</li>
							<?php } else { ?>
							<li>
								<a disabled class="cart"> <span class="glyphicon glyphicon-shopping-cart"></span></a>
								<div class="dropdown-block"></div>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
			<?php $this->load->view($content_id);?>
			<div class="footer">
				FOOTER
			</div>
		</body>
		<script type="text/javascript">
			$(document).ready(function(){
				
				$('#loginBtn').on("click",function(e){
					if($('#usr').val() == "" && $('#pwd').val() == ""){
						alert("Kolom username dan password harus diisi.");
					} else {
						$.ajax({
							type: "POST",
							url: "<?= base_url()?>/Main/do_Login",
							data: $("#formLogin").serialize(),
							dataType: "json",
							success: function(data){
								console.log(data);
								if(data.status == "OK"){
									window.location.reload();
								}else{
									alert( data.hasil );
								}
							}
						});
					}
					return false;
				});

				$('#logoutBtn').on("click",function(e){
					$.ajax({
						type: "POST",
						url: "<?= base_url()?>/Main/do_Logout",
						data: "",
						dataType: "html",
						success: function(data){
							window.location.reload();
						}
					});
					return false;
				});

				$('.dropdown').click(function(e){
					$('.dropdown-panel').removeClass("visible");
					var panel = $(this).parent().children('.dropdown-panel');
					if(panel.hasClass("visible")){
						panel.removeClass("visible");
					} else {
						panel.addClass("visible");
					}
					e.stopPropagation();
				});

				$('.dropdown-panel').click(function(e){
					e.stopPropagation();
				})

				$('body').click(function(e){
					$('.dropdown-panel').removeClass("visible");
				});

				loadCart();
				$("#cartItemCount").html(getCart().length)
			});

			function redirect(page){
				var split = window.location.href.split("/")
				var location = split[split.length-1];
				var url = "";
				for (var i = 0; i < split.length-1; i++) {
					url = url + split[i] + "/"
				}
				window.location.replace(url+page);
			}
		</script>
		<script type="text/javascript" src="<?=base_url()?>file/js/quantity-input.js"></script>
		<script type="text/javascript" src="<?=base_url()?>file/js/shopping-cart.js"></script>
	</head>
</html>