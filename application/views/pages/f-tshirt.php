
<div class="page-wrapper container-fluid" style="margin-top: 55px;">
	<div class="container">
		<p class="block-title text-center" style="border-bottom: 1px solid #f7b111;">Pesan Kaos TShirt</p>
		<div class="row">
			<div class="col-md-8">
				<div class="form-container" style="border: 1px solid lightgrey">
					<form id="tshirt-form">
						<div class="formsection">
							<div class="row sectiontitle"><span>Bahan</span></div>
							<div class="row form-group form-horizontal chooser">
								<?php foreach($bahan as $row){?>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						    		<div class="chooser-item selected">
						                <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
						    				<span class="title" style="font-size: 1.1em;"><?php echo $row->nama;?></span>
						    				<span class="description"><?php echo $row->description;?></span>
						    				<input type="radio" name="bahan" value="<?php echo $row->id;?>" checked="checked">
						    			</div>
						    			<div class="clear"></div>
						    		</div>
						    	</div>
						    	<?php } ?>
							</div>
						</div>
						<div class="formsection">
							<div class="row sectiontitle"><span>Ukuran</span></div>
							<div class="row form-group form-horizontal">
								<div class="col-sm-8">
									<select class="form-control" name="ukuran">
										<?php foreach($ukuran as $row){?>
										<option value="<?php echo $row->id;?>"><?php echo $row->nama;?> (<?php echo $row->kode;?>) </option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="formsection">
							<div class="row sectiontitle"><span>Warna</span></div>
							<div class="row form-group form-horizontal chooser">
								<?php $i=0; foreach($warna as $row){ $i++; if($i==1){$sel="selected";}else{$sel="";}?>
								<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						    		<div class="chooser-item <?php echo $sel;?>" style="padding: 0px;">
						                <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12" style="padding: 0px;">
						    				<div style="width: 40px; height: 40px; background-color: #<?php echo $row->hex_code;?>"></div>
						    				<input type="radio" name="warna" value="<?php echo $row->id_warna;?>" checked="checked">
						    			</div>
						    			<div class="clear"></div>
						    		</div>
						    	</div>
						    	<?php } ?>
							</div>
						</div>
						<div class="formsection">
							<div class="row sectiontitle"><span>Unggah berkas</span></div>
							<div class="row form-group form-horizontal">
								<a href="http://drive.google.com">Google Drive</a><br>
								<p>Upload file anda ke Google Drive. Caranya masuk ke website atau aplikasi Google Drive lalu upload file anda. Setelah selesai diupload, pilih file yang diupload dan klik 'Share Link', copy linknya lalu paste di kolom dibawah ini.</p>
								<div class="col-sm-8">
									<label for="fileurl">File URL:</label>
									<input type="text" class="form-control" name="fileurl" id="fileurl">
								</div>
							</div>
						</div>
						<div class="formsection">
							<div class="row sectiontitle"><span>Jumlah pesanan</span></div>
							<div class="row form-group form-horizontal">
								<div class="input-group col-xs-6 col-sm-6 col-md-3 col-lg-3">
									<span class="input-group-btn">
										<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant">
											<span class="glyphicon glyphicon-minus"></span>
										</button>
									</span>
									<input type="text" name="quant" class="form-control input-number" value="1" min="1" max="1000" style="text-align: center;">
									<span class="input-group-btn">
										<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant">
											<span class="glyphicon glyphicon-plus"></span>
										</button>
									</span>
								</div>
							</div>
						</div>
						<div class="formsection finishbutton">
							<div class="row form-group form-horizontal">
								<button class="btn btn-default" id="addtocart">Tambahkan ke keranjang</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-4">
				<div class="tshirt-summary" style="border: 1px solid lightgrey">
					<div class="sectiontitle"><span>Ringkasan</span></div>
					<img src="file/images/stock/plain-tshirt.png" style="width: 100%">
					<div class="container-total">
						<span>TOTAL:</span>
						<span class="right">IDR <span id="totalHarga"></span></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		updateTotalPrice()
		$(':input').change(function(){
			updateTotalPrice();
		});

		$('.chooser').find('input[type="radio"]:first').prop("checked",true);
		$('div.chooser').find('div.chooser-item').on('click', function(){
			$(this).parent().parent().find('div.chooser-item').removeClass('selected');
			$(this).addClass('selected');
			$(this).parent().parent().find('input[type="radio"]').prop("checked",false);
			$(this).find('input[type="radio"]').prop("checked", true);

			console.log($("#tshirt-form").find("[name=warna]:checked").val())
		});

		$('#addtocart').click(function(e){
			e.preventDefault();
			var isLogin = "<?php echo $this->session->userdata('is_login'); ?>";
			if(isLogin == 1){
				var bahan = $("#tshirt-form").find("[name=bahan]").val()
				var ukuran = $("#tshirt-form").find("[name=ukuran]").val()
				var warna = $("#tshirt-form").find("[name=warna]:checked").val()
				var fileurl = $("#tshirt-form").find("[name=fileurl]").val()
				var jumlah = $("#tshirt-form").find("[name=quant]").val()
				var id = guid();
				var valid = true;
				if(fileurl == ""){
					alert("File URL harus diisi.");
				} else {
					var data = {"type":"tshirt","bahan":bahan,"ukuran":ukuran,"warna":warna,"fileurl":fileurl,"jumlah":jumlah,"id":id};
					console.log(data);
					addtoCart(data);
					alert("Berhasil menambahkan ke keranjang");
					redirect("");
				}
			} else {
				alert("ERROR: Login terlebih dahulu untuk bisa menggunakan shopping cart.");
			}
		});
	});
	function updateTotalPrice(){
		var type = "tshirt";
		var bahan = $("#tshirt-form").find("[name=bahan]").val()
		var ukuran = $("#tshirt-form").find("[name=ukuran]").val()
		$.ajax({
			url: '<?=base_url()?>tshirt/getPrice',
			type: 'POST',
			dataType: 'json',
			data: {'type':type,'bahan':bahan,'ukuran':ukuran},
			success: function(data) {
				var total = (parseInt(data.bahan) + parseInt(data.ukuran)) * parseInt($("#tshirt-form").find("[name=quant]").val());
				$('#totalHarga').html(total);
			}
		});
	}
</script>