<div class="page-wrapper container-fluid" style="margin-top: 55px;">
	<div class="container">
		<?php if($isLoggedIn){ ?>
		<p class="block-title text-center">Orders</p>
		<div class="row">
			<div class="col-md-12">
				<table class="table">
					<tr>
						<th>ORDER CODE</th>
						<th>ORDER ITEMS</th>
						<th>DATE ORDERED</th>
						<th>ORDER STATUS</th>
					</tr>
					<?php foreach($orders as $row){?>
					<tr valign="top">
						<td><?php echo $row['id'];?></td>
						<td>
						<?php foreach($row['items'] as $items) { ?>
							<div>
								<span><?php echo $items['jumlah']." x ".$items['productname'];?></span>
							</div>
						<?php } ?>
						</td>
						<td><?php echo date("j F Y", strtotime($row['tanggal']));?></td>
						<td>
						<?php 
							if($row['status'] == "payment"){
								echo "Waiting for payment";
							} else {
								echo $row['status'];
							}
						?>
						</td>
					</tr>
					<?php } ?>
				</table>
			</div>
		</div>
		<?php } else { ?>
		<div style="padding-top: 200px;">
			<span style="font-size: 5em; text-align: center; width: 100%;" class="glyphicon glyphicon-remove-sign"></span>
			<p class="block-title text-center">Please Login First.</p>
		</div>
		<div class="form-container" style="height: -webkit-fill-available;" ></div>
		<?php } ?>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		
	});
</script>