
<div class="page-wrapper container-fluid" style="margin-top: 55px;">
	<div class="container" style="height: -webkit-fill-available;">
		<?php if($this->session->userdata('is_login')){ ?>
		<p class="block-title text-center" style="border-bottom: 5px solid #f7b111">Shopping Cart</p>
		<div class="row">
			<div class="col-md-12">
				<div id="keranjangkosong">
					<div style="margin: auto; width: fit-content; padding-top: 60px;">
						<span style="font-size: 1.2em;">Keranjang belanjaan anda kosong!</span>
					</div>
				</div>
				<div id="tabelkeranjang">
					<table class="table" id="cartTable">
						<tr><th style='width: 250px;'>Product</th><th></th><th style='width: 100px; text-align: center;'>Jumlah</th><th style='width: 100px; text-align: center;'>Total</th><th style='width: 150px;'></th></tr>
					</table>

					<div>
						<div class="totalContainer" style="width: 30%; max-width: 300px; min-width: 100px; float: right;">
							<div style="padding: 10px 0px; border-bottom: 1px solid lightgrey; margin-bottom: 20px;">
								<span>TOTAL</span>
								<span style="float: right;">IDR <span id="totalCart"></span></span>
							</div>
							<a id="checkoutBtn" class="btn btn-default" href="<?=base_url().'checkout'?>" style="width: 100%;">CHECKOUT</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php } else { ?>
		<div style="padding-top: 200px;">
			<span style="font-size: 5em; text-align: center; width: 100%;" class="glyphicon glyphicon-remove-sign"></span>
			<p class="block-title text-center">Login terlebih dahulu untuk bisa menggunakan shopping cart.</p>
		</div>
		<?php } ?>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		refreshTable();
	});

	$( document ).ajaxComplete(function(){
		var price = document.getElementsByClassName("cartItemPrice");
		var total = 0;
		for (var i = 0; i < price.length; i++) {
			total = total + parseInt(price[i].innerHTML);
		}
		$("#totalCart").html(total);
	});

	function deleteCartItem(index){
		if(confirm("Hapus item ini dari keranjang?")){
			deleteItem(index);
			refreshTable();
		}
	}

	function refreshTable(){
		//clearCart()
		$('#cartTable').html("<tr><th style='width: 250px;'>Product</th><th></th><th style='width: 100px; text-align: center;'>Jumlah</th><th style='width: 100px; text-align: center;'>Total</th><th style='width: 150px;'></th></tr>");
		$('#keranjangkosong').hide();
		$('#tabelkeranjang').hide();
		if(loadCart()){
			var cart = getCart();
			console.log(cart);
			if(cart.length > 0 ){
				$('#tabelkeranjang').show();
				for (var i = 0; i < cart.length; i++) {
					if(cart[i].type == "tshirt"){
					$.ajax({
						url: '<?=base_url()?>cart/loadCartTable',
						type: 'POST',
						dataType: 'json',
						data: {'type':cart[i].type,'bahan':cart[i].bahan,'ukuran':cart[i].ukuran,'warna':cart[i].warna,'jumlah':cart[i].jumlah,'cartid':cart[i].id},
						success: function(data) {
							console.log(data)
							var tr = document.createElement("tr");
							var td = document.createElement("td");
							var img = document.createElement("img");
							img.src = "file/images/stock/"+data.product[0].gambarPlain;
							img.width = "200";
							td.appendChild(img);
							tr.appendChild(td);

							td = document.createElement("td");
							var span1 = document.createElement("span")
							span1.appendChild(document.createTextNode("T-Shirt"));
							span1.style.display = "block";
							var span2 = document.createElement("span")
							span2.appendChild(document.createTextNode(data.bahan[0].nama+" - "+data.ukuran[0].kode));
							var span3 = document.createElement("span").appendChild(document.createTextNode("Warna:"));
							var divwarna = document.createElement("div")
							divwarna.style.background = '#'+data.warna[0].hex_code
							divwarna.className = "warna";
							var div = document.createElement("div")
							div.appendChild(span3);
							div.appendChild(divwarna);
							td.appendChild(span1);
							td.appendChild(span2);
							td.appendChild(div);
							tr.appendChild(td);

							td = document.createElement("td");
							var txt = document.createTextNode(data.jumlah+" pcs");
							td.appendChild(txt);
							var att = document.createAttribute('align');
							att.value = "center";
							td.setAttributeNode(att);
							tr.appendChild(td);

							var total = (parseInt(data.bahan[0].harga) + parseInt(data.ukuran[0].harga)) * parseInt(data.jumlah);
							td = document.createElement("td");
							span1 = document.createElement("span");
							span1.appendChild(document.createTextNode(total));
							span1.className = "cartItemPrice";
							txt = document.createTextNode("IDR ");
							td.appendChild(txt);
							td.appendChild(span1);
							att = document.createAttribute('align');
							att.value = "center";
							td.setAttributeNode(att);
							tr.appendChild(td);

							td = document.createElement("td");
							var btn1 = document.createElement("button");
							btn1.innerHTML = "Hapus";
							btn1.className = "btn btn-default";
							att = document.createAttribute('onclick');
							att.value = "deleteCartItem('"+data.cartid+"')";
							btn1.setAttributeNode(att);
							td.appendChild(btn1);
							att = document.createAttribute('align');
							att.value = "center";
							td.setAttributeNode(att);
							tr.appendChild(td);

							tr.className = "cartItem-row";
							document.getElementById("cartTable").appendChild(tr);
						}
					});
					}
				}
			} else {
				$('#keranjangkosong').show();
			}
		} else {
			$('#keranjangkosong').show();
		}
	}
</script>