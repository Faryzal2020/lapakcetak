<div class="page-wrapper container-fluid" style="margin-top: 55px;">
	<div class="container">
		<?php if($isLoggedIn){ ?>
		<p class="block-title text-center">Checkout</p>
		<div class="progress">
			<div class="progress-bar" id="progress" style="width:50%">Informasi Pengiriman</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div id="payment-container" class="collapse" style="padding: 50px;">
					<div style="width: 78%; display: inline-block;">
						<p>Mohon cek kembali detail informasi yang anda masukkan agar kami dapat mengantarkan pesanan anda sampai tujuan dengan cepat dan mudah.</p>
						<p>Setelah membuat pesanan, anda harus transfer ke rekening bank berikut dalam waktu kurang dari 24 jam dengan jumlah sesuai dengan yang tercantum.</p>
					</div>
					<div style="width: 50%; display: block; padding: 20px; margin: 0 auto; text-align: center;">
						<img src="https://upload.wikimedia.org/wikipedia/id/thumb/2/29/Bank_Syariah_Mandiri_logo.svg/1200px-Bank_Syariah_Mandiri_logo.svg.png" style="width: 50%">
						<span style="font-weight: 600; font-size: 1.2em; display: block;">7080012572</span>
						<span style="font-weight: 600; font-size: 1.2em; display: block;">A/N Fakhrizal Andyko</span>
					</div>
					<div class="formsection finishbutton">
						<div class="row form-group form-horizontal">
							<button tyle="button" class="btn btn-md btn-danger" data-toggle="collapse" data-target="#payment-container" id="backButton">< Kembali</button>
							<a class="btn btn-default" id="orderButton">BUAT PESANAN</a>
						</div>
					</div>
				</div>
				<div id="form-container" class="form-container">
					<form id="address-form" class="form-inline">
						<div class="formsection">
							<div class="row sectiontitle"><span>Informasi Pengiriman</span></div>
							<div class="form-group">
								<label class="control-label" for="selectAddress">Pilih Alamat</label>
								<select class="form-control" id="selectAddress" name="selectAddress">
									<option value="new">Alamat Baru</option>
									<?php foreach($addressList as $r){?>
										<option value="<?php echo $r->id; ?>"><?php echo $r->nama_alamat; ?></option>
									<?php }?>
								</select>
							</div>
							<div class="form-group" id="alamatBaru">
								<label class="control-label" for="namaAlamat">Nama Alamat</label>
								<input type="text" class="form-control" id="namaAlamat" name="namaAlamat" placeholder="Contoh: Rumah, Kantor, Rumah 2, dll.">
							</div>
							<div id="addressInput">
								<div class="form-group">
									<label class="control-label" for="nama">Nama Penerima</label>
									<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Penerima">
								</div>
								<div class="form-group">
									<label class="control-label" for="telp">Nomor Telepon</label>
									<input type="text" class="form-control" id="telp" name="telp" placeholder="Nomor Telepon">
								</div>
								<div class="form-group">
									<label class="control-label" for="alamat">Alamat</label>
									<textarea class="form-control" rows="2" id="alamat" name="alamat" placeholder="Alamat"></textarea>
								</div>
								<div class="form-group">
									<label class="control-label" for="provinsi">Provinsi</label>
									<input type="text" class="form-control" id="provinsi" name="provinsi" placeholder="Provinsi">
								</div>
								<div class="form-group">
									<label class="control-label" for="kota">Kota</label>
									<input type="text" class="form-control" id="kota" name="kota" placeholder="Kota">
								</div>
								<div class="form-group">
									<label class="control-label" for="kecamatan">Kecamatan</label>
									<input type="text" class="form-control" id="kecamatan" name="kecamatan" placeholder="Kecamatan">
								</div>
								<div class="form-group">
									<label class="control-label" for="kodepos">Kode Pos</label>
									<input style="width: 10%;" type="text" class="form-control" id="kodepos" name="kodepos" placeholder="Kode Pos">
								</div>
							</div>
						</div>
						<div class="formsection finishbutton">
							<div class="row form-group form-horizontal">
								<a class="btn btn-default" data-toggle="collapse" data-target="#payment-container" id="continueButton">LANJUTKAN KE INFO PEMBAYARAN</a>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-4">
				<div class="tshirt-summary" style="border: 1px solid lightgrey">
					<div class="sectiontitle"><span>Ringkasan</span></div>
					<div id="ringkasan-container">
						
					</div>
					<table id="cost-info" style="width: 100%;">
						<tr>
							<td class="text">SUBTOTAL</td><td class="text2">IDR <span id="subtotal"></span></td>
						</tr>
						<tr>
							<td class="text">PENGIRIMAN</td><td class="text2">IDR <span id="ongkir"></span></td>
						</tr>
						<tr>
							<td class="text">TOTAL</td><td class="text2 total">IDR <span id="total"></span></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<?php } else { ?>
		<div style="padding-top: 200px;">
			<span style="font-size: 5em; text-align: center; width: 100%;" class="glyphicon glyphicon-remove-sign"></span>
			<p class="block-title text-center">Login terlebih dahulu untuk bisa checkout.</p>
		</div>
		<div class="form-container" style="height: -webkit-fill-available;" ></div>
		<?php } ?>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		refreshTable();

		$('#continueButton').click(function(){
			$('#form-container').addClass("collapse");
			$('#progress').css("width","100%");
			$('#progress').html("Informasi Pembayaran");
		});

		$('#backButton').click(function(){
			$('#form-container').removeClass("collapse");
			$('#payment-container').addClass("collapse");
			$('#progress').css("width","50%");
			$('#progress').html("Informasi Pengiriman");
		})

		$('#selectAddress').change(function(){
			var id = $(this).val();
			$("input[type=text], textarea").val("");
			if(id != "new"){
				$('#alamatBaru').hide();
				$('#addressInput input, textarea').attr("disabled",true);
				$.ajax({
					url: '<?=base_url()?>checkout/loadAddressData',
					type: 'POST',
					dataType: 'json',
					data: {'id':id},
					success: function(data) {
						console.log(data)
						$("#nama").val(data[0].nama_penerima);
						$("#telp").val(data[0].no_telp);
						$("#alamat").val(data[0].alamat);
						$("#provinsi").val(data[0].provinsi);
						$("#kota").val(data[0].kota);
						$("#kecamatan").val(data[0].kecamatan);
						$("#kodepos").val(data[0].kode_pos);
					}
				});
			} else {
				$('#alamatBaru').show();
				$('#addressInput input, textarea').attr("disabled",false);
			}
		});

		$('#orderButton').click(function(){
			if(confirm("Klik OK untuk membuat pesanan.")){
				var cart = getCart();
				var data = {
					'dataAlamat':$("#selectAddress").val(),
					'namaAlamat':$("#namaAlamat").val(),
					'nama':$("#nama").val(),
					'telp':$("#telp").val(),
					'alamat':$("#alamat").val(),
					'provinsi':$("#provinsi").val(),
					'kota':$("#kota").val(),
					'kecamatan':$("#kecamatan").val(),
					'kodepos':$("#kodepos").val(),
					'cart': cart
				};
				console.log(data);
				data = JSON.stringify(data);
				$.ajax({
					url: '<?=base_url()?>checkout/createOrder',
					type: 'POST',
					cache: false,
					dataType: 'json',
					data: {'data':data},
					success: function(data) {
						console.log(data);
						alert("Pesanan berhasil dibuat.");
						clearCart();
						redirect("");
					}
				});
			}
		});
	});

	$( document ).ajaxComplete(function(){
		var price = document.getElementsByClassName("harga");
		var subtotal = 0;
		for (var i = 0; i < price.length; i++) {
			var int = parseInt(price[i].innerHTML.split(' ')[1])
			subtotal = subtotal + int;
		}
		$("#subtotal").html(subtotal);
		var ongkir = 18000;
		$("#ongkir").html(ongkir);
		var total = subtotal + ongkir;
		$("#total").html(total);
	});

	function refreshTable(){
		//clearCart()
		if(loadCart()){
			var cart = getCart();
			if(cart.length > 0 ){
				for (var i = 0; i < cart.length; i++) {
					if(cart[i].type == "tshirt"){
					$.ajax({
						url: '<?=base_url()?>cart/loadCartTable',
						type: 'POST',
						dataType: 'json',
						data: {'type':cart[i].type,'bahan':cart[i].bahan,'ukuran':cart[i].ukuran,'warna':cart[i].warna,'jumlah':cart[i].jumlah,'cartid':cart[i].id},
						success: function(data) {
							var total = (parseInt(data.bahan[0].harga) + parseInt(data.ukuran[0].harga)) * parseInt(data.jumlah);

							var rowdiv = document.createElement("div");
							rowdiv.className = "row";

							var div = document.createElement("div");
							div.className = "gambar";
							var img = document.createElement("img");
							img.src = "file/images/stock/plain-tshirt.PNG";
							div.appendChild(img);
							rowdiv.appendChild(div);

							div2 = document.createElement("div");
							var span = document.createElement("span");
							span.appendChild(document.createTextNode("T-Shirt"));
							span.className = "itemname"
							div2.appendChild(span);

							span = document.createElement("span");
							span.appendChild(document.createTextNode("Jumlah"));
							span2 = document.createElement("span");
							span2.appendChild(document.createTextNode(data.jumlah));
							span2.className = "jumlah";
							div2.appendChild(span);
							div2.appendChild(span2);
							span = document.createElement("span");
							span.appendChild(document.createTextNode("Harga"));
							span2 = document.createElement("span");
							span2.appendChild(document.createTextNode("IDR "+total));
							span2.className = "harga";
							div2.appendChild(span);
							div2.appendChild(span2);
							div2.style.width = "100%";

							div = document.createElement("div");
							div.className = "info";
							div.appendChild(div2);
							rowdiv.appendChild(div);
							document.getElementById("ringkasan-container").appendChild(rowdiv);
						}
					});
					}
				}
			} else {
				redirect("");
			}
		} else {
			redirect("");
		}
	}
</script>