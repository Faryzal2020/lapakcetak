<style>
@import url(http://fonts.googleapis.com/css?family=Roboto);
/*--------------------------------Button starts--------------------------------*/
.button-container {
  text-align: center;
  height: 65px;
}
.button {
  position: relative;
  background: #f7b111;
  border: #f7b111;
  font-size: 2rem;
  color: #D81900;
  margin: 3rem 0;
  padding: 0.75rem 3rem;
  cursor: pointer;
  -webkit-transition: background-color 0.28s ease, color 0.28s ease, box-shadow 0.28s ease;
  transition: background-color 0.28s ease, color 0.28s ease, box-shadow 0.28s ease;
  overflow: hidden;
  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
}
.button span {
  color: #fff;
  position: relative;
  z-index: 1;
}
.button::before {
  content: '';
  position: absolute;
  background: #071017;
  border: 50vh solid #1d4567;
  width: 30vh;
  height: 30vh;
  border-radius: 50%;
  display: block;
  top: 50%;
  left: 50%;
  z-index: 0;
  opacity: 1;
  -webkit-transform: translate(-50%, -50%) scale(0);
          transform: translate(-50%, -50%) scale(0);
}
.button:hover {
  color: #D81900;
  box-shadow: 0 6px 10px 0 rgba(0, 0, 0, 0.14), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 3px 5px -1px rgba(0, 0, 0, 0.2);
  text-decoration: none;
}
.button:active::before, .button:focus::before {
  -webkit-transition: opacity 0.28s ease 0.364s, -webkit-transform 1.12s ease;
  transition: opacity 0.28s ease 0.364s, -webkit-transform 1.12s ease;
  transition: transform 1.12s ease, opacity 0.28s ease 0.364s;
  transition: transform 1.12s ease, opacity 0.28s ease 0.364s, -webkit-transform 1.12s ease;
  -webkit-transform: translate(-50%, -50%) scale(1);
          transform: translate(-50%, -50%) scale(1);
  opacity: 0;
}
.button:focus {
  outline: none;
}
/*--------------------------------Button ends--------------------------------*/
</style>
<div class="page-wrapper container-fluid" style="margin-top: 55px;">
	<div class="carousel-container" style="height: 400px; border: 1px solid gray;">
		<div class="inner-carousel">
			<div class="item">
				<div class="slider-background" style="background-image: url('<?=base_url()?>file/images/stock/carousel-1.jpg')">
				</div>
			</div>
		</div>
	</div>
	<div class="features">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<ul>
						<li class="feature-icon"><span class="glyphicon glyphicon-time"></span></li>
						<li class="feature-info">
							<p class="feature-title">EFISIEN</p>
							<p class="feature-desc">Tidak perlu keluar rumah dan antri.</p>
						</li>
					</ul>
				</div>
				<div class="col-sm-3">
					<ul>
						<li class="feature-icon"><span class="glyphicon glyphicon-cloud-upload"></span></li>
						<li class="feature-info">
							<p class="feature-title">MUDAH</p>
							<p class="feature-desc">Tinggal upload desain.</p>
						</li>
					</ul>
				</div>
				<div class="col-sm-3">
					<ul>
						<li class="feature-icon"><span class="glyphicon glyphicon-check"></span></li>
						<li class="feature-info">
							<p class="feature-title">TERJAMIN</p>
							<p class="feature-desc">100% Garansi pengembalian.</p>
						</li>
					</ul>
				</div>
				<div class="col-sm-3">
					<ul>
						<li class="feature-icon"><span class="glyphicon glyphicon-flash"></span></li>
						<li class="feature-info">
							<p class="feature-title">CEPAT</p>
							<p class="feature-desc">Proses cetak selesai dalam 24 jam.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<p class="block-title text-center">Our Products</p>
		<div class="row">
			<?php foreach($products as $row){?>
			<div class="col-md-4 col-xs-6 col-sm-4 mb25 product-container">
				<div class="image">
				<img src="file/images/stock/<?php echo $row->gambar;?>" height="300" width="356" >
				</div>
				<div class="info">
					<?php echo $row->nama_produk;?>
				</div>
				<div class="button-container">
					<a href="<?=base_url().$row->link?>" class="button"><span>Buat Sekarang</span></a>
				</div> 
			</div>
			<?php } ?>
		</div>
	</div>
	
</div>