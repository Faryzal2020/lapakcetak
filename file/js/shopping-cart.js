function loadCart(){
	if(!localStorage.cart){
		var cart = [];
		localStorage.setItem("cart",JSON.stringify(cart));
		return false;
	} else {
		return true;
	}
}

function addtoCart(data){
	var cart = getCart();
	cart.push(data);
	localStorage.setItem("cart",JSON.stringify(cart));
}

function getCart(){
	return JSON.parse(localStorage.cart);
}

function clearCart(){
	localStorage.removeItem("cart");
}

function deleteItem(index){
	var cart = getCart();
	var newcart = []
	for (var i = 0; i < cart.length; i++) {
		console.log(cart[i]);
		if(cart[i].id != index){
			newcart.push(cart[i]);
		}
	}
	console.log(cart);
	localStorage.setItem("cart",JSON.stringify(newcart));
}

function guid() {
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

function s4() {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
}